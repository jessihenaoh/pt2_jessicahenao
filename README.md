## Programmation Concurrente

---
#### TP 2 - Tutoriel Python 3 - 4

En exécutant les différents exemples des tutoriels 3 et 4, je n'ai eu aucun problème.

Juste une question, quand je travaille python comme calculatrice (tutoriel 3 - première partie) dois-je toujours faire print () Ou attribuer à une variable? 
Car sinon ça ne marche pas.

###### Par example
![example](ex.png)