# Tutoriel 3
# Calculatrice
a = 2 + 2
b = 50 - 5*6
c = 8 / 5
d = 17 // 3
e = 17 / 3
f = 17 % 3
print(a, b, c, d, e, f)

# Variable definie
width = 20
height = 5 * 9
g = width * height
print(g)

# variable _
#tax = 12.5 / 100
#price = 100.50
#price * tax
#price + _
#print(round(_, 2))

### Problèmes de travail en python (calculatrice), car cela me pose toujours un problème ou ne reconnaît pas les opérations, donc je dois attribuer une variable et imprimer pour connaître les résultats

# Les chaînes de caractères
print('spam eggs')  # single quotes
print('doesn\'t')  # use \' to escape the single quote...
print("doesn't") # ...or use double quotes instead
print('"Yes," he said.')
print("\"Yes,\" he said.")
print('"Isn\'t," she said.')

s = 'First line.\nSecond line.'
print(s)

print(r'C:\some\name')

print("""\
Usage: thingy [OPTIONS]
     -h                        Display this usage message
     -H hostname               Hostname to connect to
""")

#
a = 3 * 'un' + 'ium'
print (a)

print ('Py' 'thon')

prefix = 'Py'
#prefix 'thon' #SyntaxError: invalid syntax
print (prefix + 'thon')

text = ('Put several strings within parentheses '
        'to have them joined together.')
print (text)

#position (négatifs et positifs)
word = 'Python'
print (word[0])
print (word[5])
print (word[-6])
print (word[-1])
print (word[0:2])
print (word[:2] + word[2:])

#print (word[42]) #IndexError: string index out of range

print (word[4:42])
print (word[42:])

#word[0] = 'J' #TypeError: 'str' object does not support item assignment

#word[2:] = 'py'
#print (word[2:]) #TypeError: 'str' object does not support item assignment

print('J' + word[1:])
print (word[:2] + 'py')

#longueur d’une chaîne "len()"

s = 'supercalifragilisticexpialidocious'
print (len(s))

#Listes

squares = [1, 4, 9, 16, 25]
print (squares[0])
print (squares[-1])
print (squares[-3:])
print (squares[:]) #tout

print (squares + [36, 49, 64, 81, 100])

#changer leur contenu
cubes = [1, 8, 27, 65, 125]
print(cubes)
cubes[3] = 64
print(cubes)

#ajouter (.append())
cubes.append(216)
cubes.append(7**3)
print(cubes)

letters = ['a', 'b', 'c', 'd', 'e', 'f', 'g']
print(letters)
letters[2:5] = ['C', 'D', 'E']
print(letters)
letters[2:5] = []
print(letters)
letters[:] = []
print (letters)
letters = ['a', 'b', 'c', 'd']
print(len(letters))

#listes contenant d’autres listes
a = ['a', 'b', 'c']
n = [1, 2, 3]
x = [a, n]
print (x)
print (x[0])
print (x[0][1])

#fibonacci
#utilisation (while)

a, b = 0, 1
while b < 10:
        print(b)
        a, b = b, a+b

a, b = 0, 1
while b < 10:
     print(b, end=',')
     a, b = b, a+b

#utilisation (if)
x = int(input("Please enter an integer: "))
if x < 0:
    x = 0
    print('Negative changed to zero')
elif x == 0:
    print('Zero')
elif x == 1:
    print('Single')
else:
    print('More')

#utilisation (for)
words = ['cat', 'window', 'defenestrate']
for w in words:
    print(w, len(w))

for w in words[:]:
    if len(w) > 6:
        words.insert(0, w)
print (words)

#Fonction (range())
print("Range:")
for i in range(5):
    print(i)

print("Range:(5,10)")
for i in range(5,10):
    print(i)

print ("Range: (0, 10, 3)")
for i in range(0,10, 3):
    print(i)

print ("range(-10, -100, -30)")
for i in range(-10, -100, -30):
    print(i)

a = ['Mary', 'had', 'a', 'little', 'lamb']
for i in range(len(a)):
    print(i, a[i])


print(range(10)) #range() se comportent presque comme des listes, pour imprimer range (10), il faut utiliser des iterateurs

#list()
print(list(range(5)))

#instructions break et continue
print ("break")
for n in range(2, 10):
    for x in range(2, n):
        if n % x == 0:
            print(n, 'equals', x, '*', n//x)
            break
        else:
            print(n, 'is a prime number')

print ("continue")
for num in range(2, 10):
    if num % 2 == 0:
        print("Found an even number", num)
        continue
    print("Found a number", num)

#Définir des fonctions
#fib avec print
def fib(n):
    a, b = 0, 1
    while a < n:
        print(a, end=' ')
        a, b = b, a+b
    print()

fib(200)

f = fib
f(10)

fib(0)
print(fib(0))

#fib avec Return et append()

def fib2(n):
    result = []
    a, b = 0, 1
    while a < n:
        result.append(a)
        a, b = b, a+b
    return result
f100= fib2(100)
print(f100)

#arguments
def ask_ok(prompt, retries=4, reminder='Please try again!'):
    while True:
        ok = input(prompt)
        if ok in ('y', 'ye', 'yes'):
            return True
        if ok in ('n', 'no', 'nop', 'nope'):
            return False
        retries = retries - 1
        if retries < 0:
            raise ValueError('invalid user response')
        print(reminder)

i = 5

def f(arg=i):
    print(arg)
i = 6
f()

def f(a, L=[]):
    L.append(a)
    return L

print(f(1))
print(f(2))
print(f(3))


def f(a, L=None):
    if L is None:
        L = []
    L.append(a)
    return L

#arguments nommés

def parrot(voltage, state='a stiff', action='voom', type='Norwegian Blue'):
    print("-- This parrot wouldn't", action, end=' ')
    print("if you put", voltage, "volts through it.")
    print("-- Lovely plumage, the", type)
    print("-- It's", state, "!")

#différentes manières d'envoyer les arguments
parrot(1000)                                          # 1 positional argument
parrot(voltage=1000)                                  # 1 keyword argument
parrot(voltage=1000000, action='VOOOOOM')             # 2 keyword arguments
parrot(action='VOOOOOM', voltage=1000000)             # 2 keyword arguments
parrot('a million', 'bereft of life', 'jump')         # 3 positional arguments
parrot('a thousand', state='pushing up the daisies')  # 1 positional, 1 keyword

#les arguments nommés doivent suivre les arguments positionnés et doivent correspondre à l’un des arguments acceptés

def cheeseshop(kind, *arguments, **keywords):
    print("-- Do you have any", kind, "?")
    print("-- I'm sorry, we're all out of", kind)
    for arg in arguments:
        print(arg)
    print("-" * 40)
    keys = sorted(keywords.keys())
    for kw in keys:
        print(kw, ":", keywords[kw])

cheeseshop("Limburger", "It's very runny, sir.",
           "It's really very, VERY runny, sir.",
           shopkeeper="Michael Palin",
           client="John Cleese",
           sketch="Cheese Shop Sketch")


#arguments arbitraires ---- dernier paramètre que je peux modifier
def concat(*args, sep="/"):
    return sep.join(args)

print (concat("earth", "mars", "venus"))
print (concat("earth", "mars", "venus", sep="."))

#Séparation des listes d’arguments

print (list(range(3, 6)))
args = [3, 6]
print (list(range(*args)))


def parrot2(voltage, state='a stiff', action='voom'):
    print("-- This parrot wouldn't", action, end=' ')
    print("if you put", voltage, "volts through it.", end=' ')
    print("E's", state, "!")

d = {"voltage": "four million", "state": "bleedin' demised", "action": "VOOM"}
print (parrot2(**d))


#Fonctions anonymes

def make_incrementor(n):
    return lambda x: x + n

f = make_incrementor(42)
print (f(0))
print (f(1))


#Chaînes de documentation
def my_function():
    """Do nothing, but document it.
     No, really, it doesn't do anything.
     """
    pass

print(my_function.__doc__)



#Annotations de fonctions
def f(ham: str, eggs: str = 'eggs') -> str:
    print("Annotations:", f.__annotations__)
    print("Arguments:", ham, eggs)
    return ham + ' and ' + eggs

print(f('spam'))


